const { BadRequest, Unauthorize } = require("../error");
const jwt = require("jsonwebtoken");
const { StatusCodes } = require("http-status-codes");

module.exports.login = (req, res) => {
    const validation = Object.keys(req.body).reduce((acc, val) => {
        if (!req.body[val]) {
            acc.push(`${val} is mandatory!`);
        }
        return acc;
    }, []);

    if (validation.length) {
        throw new BadRequest(validation);
    }

    const id = new Date().getDate();

    const token = jwt.sign(
        {
            id,
            userName: req.body.userName,
        },
        process.env.JWT_SECRET,
        {
            expiresIn: "10h",
        }
    );
    return res.status(StatusCodes.OK).json({
        status: true,
        message: "Successfully login!",
        entity: token,
    });
};

module.exports.dashboard = (req, res) => {
    const { id, userName } = req.user;
    return res.status(StatusCodes.OK).json({
        status: false,
        entity: {
            id,
            userName,
        },
    });
};
