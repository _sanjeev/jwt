const { CustomApiError } = require("./CustomApiError");
const { StatusCodes } = require("http-status-codes");
class Unauthorize extends CustomApiError {
    constructor(message) {
        super(message);
        this.statusCode = StatusCodes.UNAUTHORIZED;
        this.status = false;
    }
}

module.exports = {
    Unauthorize,
};
