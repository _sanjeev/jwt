const { Unauthorize } = require("../error");
const jwt = require("jsonwebtoken");

module.exports.authorize = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (!authHeader || !authHeader.startsWith("Bearer ")) {
        throw new Unauthorize("No token found!");
    }

    try {
        const token = authHeader.split(" ")[1];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const { id, userName } = decoded;
        req.user = {
            id,
            userName,
        };
        next();
    } catch (error) {
        throw new Unauthorize("Unauthorize user!");
    }
};
