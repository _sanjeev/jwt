const { CustomApiError } = require("../error");
const { StatusCodes } = require("http-status-codes");

module.exports.errorHandler = (err, req, res, next) => {
    if (err instanceof CustomApiError) {
        return res.status(err.statusCode).json({
            status: false,
            message: err.message,
        });
    }
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        status: false,
        message: "Something went wrong!",
    });
};
