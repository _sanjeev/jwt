const express = require("express");
const { login, dashboard } = require("../controllers/jwtController");
const { authorize } = require("../middleware/auth");
const router = express.Router();

router.post("/login", login);
router.route("/dashboard").get(authorize, dashboard);

module.exports = router;
